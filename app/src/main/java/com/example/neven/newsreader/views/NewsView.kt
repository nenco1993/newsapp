package com.example.neven.newsreader.views

import com.example.neven.newsreader.models.Article
import com.example.neven.newsreader.models.News

interface NewsView:BaseView {

    fun showNews(listArticles:List<Article>)

}