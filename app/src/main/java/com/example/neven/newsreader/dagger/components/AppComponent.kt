package com.example.neven.newsreader.dagger.components

import com.example.neven.newsreader.activities.BaseActivity
import com.example.neven.newsreader.dagger.modules.AppModule
import com.example.neven.newsreader.dagger.modules.NetModule
import com.example.neven.newsreader.dagger.modules.NewsModule
import dagger.Component
import dagger.Module
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(NetModule::class, AppModule::class))
interface AppComponent {

    fun plus(module: NewsModule): NewsComponent


}