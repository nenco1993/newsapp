package com.example.neven.newsreader.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.neven.newsreader.R
import com.example.neven.newsreader.models.Article

class NewsAdapter(var listNews: List<Article>, var context: Context, val clickListener: NewsClickListener?) : RecyclerView.Adapter<NewsViewHolder>() {


    override fun onBindViewHolder(holder: NewsViewHolder?, position: Int) {

        val singleArticle = listNews[position]

        holder?.bindItems(singleArticle)
        holder?.itemView?.setOnClickListener {

            clickListener?.onNewsClicked(singleArticle, position)


        }


    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): NewsViewHolder {

        val view = LayoutInflater.from(parent?.context).inflate(R.layout.list_item_news, parent, false)

        return NewsViewHolder(view, context)


    }

    override fun getItemCount(): Int {

        return listNews.size

    }


    interface NewsClickListener {

        fun onNewsClicked(article: Article, position: Int)
    }
}