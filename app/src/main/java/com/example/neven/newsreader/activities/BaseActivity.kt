package com.example.neven.newsreader.activities

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.example.neven.newsreader.MyApplication
import com.example.neven.newsreader.dagger.components.AppComponent

abstract class BaseActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        injectDependencies(MyApplication.netcomponent)
    }

    abstract fun injectDependencies(appComponent: AppComponent)


}
