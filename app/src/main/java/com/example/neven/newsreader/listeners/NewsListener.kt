package com.example.neven.newsreader.listeners

import com.example.neven.newsreader.models.Article
import com.example.neven.newsreader.models.News

interface NewsListener : BaseListener{

    fun onSuccess(listArticles:List<Article>)

}