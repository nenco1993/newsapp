package com.example.neven.newsreader.presenters

import com.example.neven.newsreader.interactors.NewsInteractor
import com.example.neven.newsreader.listeners.NewsListener
import com.example.neven.newsreader.models.Article
import com.example.neven.newsreader.models.News
import com.example.neven.newsreader.views.NewsView
import javax.inject.Inject

class NewsPresenterImpl @Inject constructor(var interactor: NewsInteractor, val view: NewsView) : NewsPresenter, NewsListener {


    override fun loadNews() {

        interactor.downloadData(this)


    }

    override fun onSuccess(listArticles: List<Article>) {

        view.showNews(listArticles)

    }

    override fun onFailure() {

        view.showErrorDialog()


    }
}