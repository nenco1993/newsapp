package com.example.neven.newsreader.network

import com.example.neven.newsreader.MyApplication
import com.example.neven.newsreader.R
import com.example.neven.newsreader.utils.NetworkUtils
import okhttp3.Interceptor
import okhttp3.Response

class OnlineInterceptor(val networkUtils: NetworkUtils) : Interceptor {

    override fun intercept(chain: Interceptor.Chain?): Response {


        val originalRequest = chain!!.request()
        val cacheHeaderValue = if (networkUtils.isNetworkConnected())
            MyApplication.instance.getString(R.string.response_header_max_age)
        else
            MyApplication.instance.getString(R.string.response_header_max_stale)
        val request = originalRequest.newBuilder().build()
        val response = chain.proceed(request)


        return response.newBuilder()
                .removeHeader(MyApplication.instance.getString(R.string.response_header_pragma))
                .removeHeader(MyApplication.instance.getString(R.string.response_header_cache_control))
                .header(MyApplication.instance.getString(R.string.response_header_cache_control), cacheHeaderValue)
                .build()
    }
}