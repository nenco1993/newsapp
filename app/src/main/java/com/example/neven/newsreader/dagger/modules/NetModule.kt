package com.example.neven.newsreader.dagger.modules

import android.content.Context
import com.example.neven.newsreader.MyApplication
import com.example.neven.newsreader.R
import com.example.neven.newsreader.network.OfflineInterceptor
import com.example.neven.newsreader.network.OnlineInterceptor
import com.example.neven.newsreader.network.RestAPI
import com.example.neven.newsreader.utils.NetworkUtils
import dagger.Module
import dagger.Provides
import okhttp3.*
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.io.File
import javax.inject.Singleton

@Module
class NetModule {

    @Provides
    @Singleton
    fun provideNetworkUtils(context: Context): NetworkUtils {

        return NetworkUtils(context)


    }

    @Provides
    @Singleton
    fun provideOfflineInterceptor(networkUtils: NetworkUtils): OfflineInterceptor {

        return OfflineInterceptor(networkUtils)


    }

    @Provides
    @Singleton
    fun provideOnlineInterceptor(networkUtils: NetworkUtils): OnlineInterceptor {

        return OnlineInterceptor(networkUtils)


    }

    @Provides
    @Singleton
    fun provideOkHttp(onlineInterceptor: OnlineInterceptor, offlineInterceptor: OfflineInterceptor): OkHttpClient {

        val httpCacheDirectory: File = File(MyApplication.instance.cacheDir, MyApplication.instance.getString(R.string.cache_dir))
        val cacheSize: Long = 10 * 1024 * 1024
        val cache: Cache = Cache(httpCacheDirectory, cacheSize)


        val client: OkHttpClient = OkHttpClient().newBuilder()
                .cache(cache)
                .addNetworkInterceptor(onlineInterceptor)
                .addInterceptor(offlineInterceptor)
                .build()

        return client

    }


    @Provides
    @Singleton
    fun provideRetrofit(okHttpClient: OkHttpClient): Retrofit {

        val retrofit: Retrofit = Retrofit.Builder()
                .baseUrl(MyApplication.instance.getString(R.string.retrofit_base_url))
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(okHttpClient)
                .build()

        return retrofit


    }

    @Provides
    @Singleton
    fun provideRestAPI(retrofit: Retrofit): RestAPI {

        return retrofit.create(RestAPI::class.java)
    }

}


