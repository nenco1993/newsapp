package com.example.neven.newsreader

import android.app.Application
import com.example.neven.newsreader.dagger.components.DaggerAppComponent
import com.example.neven.newsreader.dagger.components.AppComponent
import com.example.neven.newsreader.dagger.modules.AppModule

class MyApplication : Application() {

    companion object {
        lateinit var netcomponent: AppComponent
        lateinit var instance: MyApplication

    }

    override fun onCreate() {
        super.onCreate()
        instance = this
        netcomponent = DaggerAppComponent.builder()
                .appModule(AppModule(this))
                .build()


    }
}