package com.example.neven.newsreader.interactors

import com.example.neven.newsreader.listeners.NewsListener
import com.example.neven.newsreader.models.News
import com.example.neven.newsreader.network.RestAPI
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

import javax.inject.Inject

class NewsInteractorImpl @Inject constructor(val api: RestAPI, val compositeDisposable: CompositeDisposable) : NewsInteractor {

    lateinit var observableNews: Observable<News>

    override fun downloadData(listener: NewsListener) {

        observableNews = api.getNews()
        val disposableNews: Disposable = observableNews.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        {
                            news ->
                            listener.onSuccess(news.articles!!.filterNotNull())

                        },

                        {
                            e ->
                            listener.onFailure()
                            e.printStackTrace()
                        }


                )
        compositeDisposable.add(disposableNews)


    }
}