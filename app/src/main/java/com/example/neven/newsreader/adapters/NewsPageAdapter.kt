package com.example.neven.newsreader.adapters

import android.content.Context
import android.support.v4.view.PagerAdapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.example.neven.newsreader.R
import com.example.neven.newsreader.models.Article

class NewsPageAdapter(var listNews: List<Article>, var context: Context) : PagerAdapter() {

    override fun instantiateItem(container: ViewGroup?, position: Int): Any {

        val view: View = LayoutInflater.from(context).inflate(R.layout.view_news, container, false)

        val title = view.findViewById(R.id.tvNewsDetailsTitle) as TextView
        val description = view.findViewById(R.id.tvNewsDetailsDescription) as TextView
        val picture = view.findViewById(R.id.ivNewsDetailsPicture) as ImageView

        val singleNews: Article = listNews[position]


        Glide
                .with(context)
                .load(singleNews.urlToImage)
                .placeholder(R.drawable.placeholder_news)
                .crossFade()
                .into(picture)

        title.text = singleNews.title
        description.text = singleNews.description


        container?.addView(view)

        return view

    }

    override fun isViewFromObject(view: View?, `object`: Any?): Boolean {

        return view == `object`

    }

    override fun destroyItem(container: ViewGroup?, position: Int, `object`: Any?) {

        if (`object` is View) {

            container?.removeView(`object`)
        }

    }

    override fun getCount(): Int {

        return listNews.size

    }


}