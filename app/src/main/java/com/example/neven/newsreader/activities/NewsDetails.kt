package com.example.neven.newsreader.activities


import android.os.Bundle
import android.support.v4.app.FragmentActivity
import android.support.v4.view.PagerAdapter
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import android.widget.Toolbar


import com.example.neven.newsreader.R
import com.example.neven.newsreader.adapters.NewsPageAdapter
import com.example.neven.newsreader.models.Article
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_news_details.*
import kotlinx.android.synthetic.main.content_news_details.*
import org.jetbrains.anko.toast


class NewsDetails : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_news_details)
        toolbarNewsDetails.title = ""
        setSupportActionBar(toolbar)

        if (intent != null) {

            val position: Int = intent.getSerializableExtra(getString(R.string.key_position_news)) as Int
            val listNews: List<Article> = intent.getSerializableExtra(getString(R.string.key_list_news)) as List<Article>

            val myAdapter: NewsPageAdapter = NewsPageAdapter(listNews, baseContext)
            viewpager.adapter = myAdapter
            viewpager.currentItem = position

            ivArrow.setOnClickListener {
                onBackPressed()
            }

            viewpager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {

                override fun onPageScrollStateChanged(state: Int) {


                }

                override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

                    tvToolbarTitle.text = listNews[position].title


                }

                override fun onPageSelected(position: Int) {


                }
            })


        }

    }


}
