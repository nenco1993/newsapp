package com.example.neven.newsreader.presenters

interface NewsPresenter {

    fun loadNews()
}