package com.example.neven.newsreader.activities

import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.example.neven.newsreader.R
import com.example.neven.newsreader.adapters.NewsAdapter
import com.example.neven.newsreader.dagger.components.AppComponent
import com.example.neven.newsreader.dagger.modules.NewsModule
import com.example.neven.newsreader.models.Article
import com.example.neven.newsreader.presenters.NewsPresenter
import com.example.neven.newsreader.views.NewsView
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*
import org.jetbrains.anko.alert
import java.io.Serializable
import javax.inject.Inject

class MainActivity : BaseActivity(), NewsView, NewsAdapter.NewsClickListener {

    @Inject
    lateinit var presenter: NewsPresenter

    @Inject
    lateinit var compositeDisposable: CompositeDisposable

    var adapter: NewsAdapter? = null

    var listNews: List<Article> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        toolbar.title = ""
        setSupportActionBar(toolbar)
        tvToolbarHeadline.text = getString(R.string.headline)


        val manager: LinearLayoutManager = LinearLayoutManager(this)
        recyclerView.layoutManager = manager
        recyclerView.itemAnimator = DefaultItemAnimator()


    }

    override fun injectDependencies(appComponent: AppComponent) {

        appComponent.plus(NewsModule(this)).inject(this)

    }

    override fun showNews(listArticles: List<Article>) {

        adapter = NewsAdapter(listArticles, this, this)
        recyclerView.adapter = adapter
        progressBar.visibility = View.GONE
        listNews = listArticles


    }

    override fun showErrorDialog() {

        progressBar.visibility = View.GONE

        alert(getString(R.string.dialog_error_message)) {

            title = getString(R.string.dialog_error_title)

            neutralPressed(getString(R.string.dialog_error_button), { })

        }.show()


    }

    override fun onNewsClicked(article: Article, position: Int) {

        val intent: Intent = Intent(baseContext, NewsDetails::class.java)
        val bundle: Bundle = Bundle()
        bundle.putSerializable(getString(R.string.key_list_news), listNews as Serializable)
        bundle.putInt(getString(R.string.key_position_news), position)
        intent.putExtras(bundle)
        startActivity(intent)


    }

    override fun onResume() {
        super.onResume()
        presenter.loadNews()

    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.clear()
    }


}
