package com.example.neven.newsreader.dagger.modules

import android.content.Context
import com.example.neven.newsreader.MyApplication
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Singleton

@Module
class AppModule(var app: MyApplication) {

    @Provides
    @Singleton
    fun provideAppContext(): Context {

        return app.applicationContext
    }


    @Provides
    @Singleton
    fun provideCompositeDisposables(): CompositeDisposable {

        return CompositeDisposable()
    }


}