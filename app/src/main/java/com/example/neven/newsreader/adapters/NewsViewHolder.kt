package com.example.neven.newsreader.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.View
import com.bumptech.glide.Glide
import com.example.neven.newsreader.R
import com.example.neven.newsreader.models.Article
import kotlinx.android.synthetic.main.list_item_news.view.*

class NewsViewHolder(itemView: View?, var context: Context) : RecyclerView.ViewHolder(itemView) {

    fun bindItems(article: Article?) {

        itemView.tvNewsTitle.text = article?.title

        Glide
                .with(context)
                .load(article?.urlToImage)
                .crossFade()
                .into(itemView.ivNewsLogo)


    }
}