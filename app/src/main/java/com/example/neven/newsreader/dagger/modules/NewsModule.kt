package com.example.neven.newsreader.dagger.modules

import com.example.neven.newsreader.dagger.scopes.ActivityScope
import com.example.neven.newsreader.interactors.NewsInteractor
import com.example.neven.newsreader.interactors.NewsInteractorImpl
import com.example.neven.newsreader.presenters.NewsPresenter
import com.example.neven.newsreader.presenters.NewsPresenterImpl
import com.example.neven.newsreader.views.NewsView
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
class NewsModule(val view: NewsView) {


    @Provides
    @ActivityScope
    fun provideNewsInteractor(interactor: NewsInteractorImpl): NewsInteractor {

        return interactor


    }

    @Provides
    @ActivityScope
    fun provideNewsPresenter(presenter: NewsPresenterImpl): NewsPresenter {

        return presenter
    }

    @Provides
    @ActivityScope
    fun provideNewsView(): NewsView {

        return view


    }


}