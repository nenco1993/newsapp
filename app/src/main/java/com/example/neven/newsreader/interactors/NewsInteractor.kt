package com.example.neven.newsreader.interactors

import com.example.neven.newsreader.listeners.NewsListener

interface NewsInteractor {

    fun downloadData(listener:NewsListener)

}