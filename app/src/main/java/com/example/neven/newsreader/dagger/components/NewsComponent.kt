package com.example.neven.newsreader.dagger.components

import com.example.neven.newsreader.activities.MainActivity
import com.example.neven.newsreader.dagger.modules.NewsModule
import com.example.neven.newsreader.dagger.scopes.ActivityScope
import dagger.Subcomponent

@ActivityScope
@Subcomponent(modules = arrayOf(NewsModule::class))
interface NewsComponent {

    fun inject(activity: MainActivity)
}