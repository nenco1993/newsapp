package com.example.neven.newsreader.network

import com.example.neven.newsreader.models.News
import io.reactivex.Observable
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Headers


interface RestAPI {

    @GET("articles?source=bbc-news&sortBy=top&apiKey=6946d0c07a1c4555a4186bfcade76398")
    fun getNews(): Observable<News>

}